CREATE TABLE models (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	context_length INTEGER NOT NULL,
	tokenizer VARCHAR(255) NOT NULL,
	modality VARCHAR(255) NOT NULL
);
