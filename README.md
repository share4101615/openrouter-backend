## Описание `.env`

```
COMPOSE_PROJECT_NAME=openrouter

BACKEND_PORT=3000
POSTGRES_HOSTNAME=localhost
POSTGRES_PORT=5432
POSTGRES_USER=openrouter
POSTGRES_PASSWORD=example-password
POSTGRES_DATABASE_NAME=openrouter
POSTGRES_APP_NAME=openrouter-backend
```
