import z from "zod";

type ConfigType = z.infer<typeof configSchema>;

const configSchema = z.object({
	// app
	BACKEND_PORT: z.string().transform((port) => parseInt(port, 10)),

	// postgres
	POSTGRES_HOSTNAME: z.string(),
	POSTGRES_PORT: z.string().transform((port) => parseInt(port, 10)),
	POSTGRES_USER: z.string(),
	POSTGRES_PASSWORD: z.string(),
	POSTGRES_DATABASE_NAME: z.string(),
	POSTGRES_APP_NAME: z.string(),
});

const getConfig = (): ConfigType => {
	const result = configSchema.safeParse(process.env);
	if (!result.success) {
		throw new Error("Invalid configuration. Check your .env file.");
	} else {
		return result.data;
	}
};
export { getConfig, ConfigType };
