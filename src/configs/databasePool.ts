import { Pool } from "pg";
import { ConfigType, getConfig } from "./configSchema";

const getDatabasePool = (config: ConfigType): Pool => {
	return new Pool({
		host: config.POSTGRES_HOSTNAME,
		port: config.POSTGRES_PORT,
		user: config.POSTGRES_USER,
		password: config.POSTGRES_PASSWORD,
		database: config.POSTGRES_DATABASE_NAME,
	});
};

const checkConnection = async (pool: Pool): Promise<void> => {
	const client = await pool.connect();
	try {
		const res = await client.query("SELECT NOW()");
		console.log(res.rows[0]);
	} finally {
		client.release();
	}
};

const pool = getDatabasePool(getConfig());

export { pool, checkConnection };
