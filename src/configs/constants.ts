export const OPENTOUTER_API_URL_V1 = "https://openrouter.ai/api/v1";
export const CRON_TASK = {
	EVERY_SECOND: "* * * * * *",
};
