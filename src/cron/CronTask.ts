import cron from "node-cron";
import { Task } from "./interfaces";

export class CronTasks {
	private tasks: Task[];

	constructor(tasks: Task[]) {
		this.tasks = tasks;
	}

	scheduleTask(cronTime: string, cronFn: () => void) {
		cron.schedule(
			cronTime,
			() => {
				cronFn();
			},
			{
				scheduled: true,
			},
		);
	}

	startAllTasks() {
		this.tasks.forEach((task) => this.scheduleTask(task.cronTime, task.taskFn));
	}
}
