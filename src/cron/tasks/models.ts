import { CRON_TASK } from "@/configs";
import { Task } from "../interfaces";

export const modelsTask: Task = {
	cronTime: CRON_TASK.EVERY_SECOND,
	taskFn: () => console.log("Running task 1"),
};
