import { BaseController } from "./baseController";
import { ModelsController } from "./models";

const controllers: BaseController[] = [new ModelsController()];

export { controllers };
