import { IModel } from "@interfaces";
import { pool } from "@/configs";

export class ModelsRepository {
	public async getAllModels() {
		const query = "SELECT * FROM models";
		const { rows } = await pool.query(query);
		return rows;
	}

	public async addModel(modelData: Omit<IModel, "id">) {
		const query = `
            INSERT INTO models (name, description, context_length, tokenizer, modality)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING *;
        `;
		const values = [modelData.name, modelData.description, modelData.context_length, modelData.tokenizer, modelData.modality];
		const { rows } = await pool.query(query, values);
		return rows[0];
	}

	public async getModelById(id: string) {
		const query = "SELECT * FROM models WHERE id = $1";
		const { rows } = await pool.query(query, [id]);
		return rows[0];
	}

	public async updateModelById(id: string, modelData: Partial<IModel>) {
		const fields = Object.keys(modelData)
			.map((key, index) => `${key} = $${index + 2}`)
			.join(", ");
		const query = `
            UPDATE models
            SET ${fields}
            WHERE id = $1
            RETURNING *;
        `;
		const values = [id, ...Object.values(modelData)];
		const { rows } = await pool.query(query, values);
		return rows[0];
	}

	public async deleteModelById(id: string) {
		const query = "DELETE FROM models WHERE id = $1 RETURNING *";
		const { rows } = await pool.query(query, [id]);
		return rows[0];
	}
}
