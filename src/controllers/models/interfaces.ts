// TODO: check types in documentation https://openrouter.ai/
export interface IOpenrouterAiApiV1 {
	id: string;
	name: string;
	description: string;
	pricing: ModelPricing;
	context_length: number;
	architecture: ModelArchitecture;
	top_provider: ModelTopProvider;
	per_request_limits: null;
}

interface ModelPricing {
	prompt: string;
	completion: string;
	image: string;
	request: string;
}

interface ModelArchitecture {
	modality: string;
	tokenizer: string;
	instruct_type: string | null;
}

interface ModelTopProvider {
	max_completion_tokens: number | null;
	is_moderated: boolean;
}
