import { Router, Request, Response } from "express";

import { ModelsService } from "./ModelsService";
import { BaseController } from "../baseController";

class ModelsController extends BaseController {
	public router: Router;
	private modelsService: ModelsService;

	constructor() {
		super();
		this.router = Router();
		this.modelsService = new ModelsService();
		this.initializeRoutes();
	}

	protected initializeRoutes(): void {
		this.router.get("/models", this.getAllModels.bind(this));
		this.router.post("/models", this.addModel.bind(this));
		this.router.get("/models/:id", this.getModelById.bind(this));
		this.router.put("/models/:id", this.updateModelById.bind(this));
		this.router.delete("/models/:id", this.deleteModelById.bind(this));
	}

	private async getAllModels(req: Request, res: Response) {
		const result = await this.modelsService.getAllModels();
		res.json(result);
	}

	private async addModel(req: Request, res: Response) {
		console.log(req.body);
		const result = await this.modelsService.addModel(req.body);
		res.status(201).json(result);
	}

	private async getModelById(req: Request, res: Response) {
		const { id } = req.params;
		const result = await this.modelsService.getModelById(id);
		res.json(result);
	}

	private async updateModelById(req: Request, res: Response) {
		const { id } = req.params;
		const result = await this.modelsService.updateModelById(id, req.body);
		res.json(result);
	}

	private async deleteModelById(req: Request, res: Response) {
		const { id } = req.params;
		const result = await this.modelsService.deleteModelById(id);
		res.json(result);
	}
}

export { ModelsController };
