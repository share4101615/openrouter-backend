import { OPENTOUTER_API_URL_V1 } from "@/configs";
import { IOpenrouterAiApiV1 } from "./interfaces";
import { ModelsRepository } from "./ModelsRepository";
import { IModel } from "@interfaces";

export class ModelsService {
	private repository: ModelsRepository;

	constructor() {
		this.repository = new ModelsRepository();
	}

	// Working with local database
	public getAllModels() {
		return this.repository.getAllModels();
	}

	public addModel(modelData: IModel) {
		return this.repository.addModel(modelData);
	}

	public getModelById(id: string) {
		return this.repository.getModelById(id);
	}

	public updateModelById(id: string, modelData: IModel) {
		return this.repository.updateModelById(id, modelData);
	}

	public deleteModelById(id: string) {
		return this.repository.deleteModelById(id);
	}

	// Working with fetched data
	public fetchNodels = async (): Promise<IOpenrouterAiApiV1> => {
		const response = await fetch(`${OPENTOUTER_API_URL_V1}/models`);
		if (!response.ok) {
			throw new Error(`Failed to fetch data from ${OPENTOUTER_API_URL_V1}`);
		}
		return response.json();
	};
}
