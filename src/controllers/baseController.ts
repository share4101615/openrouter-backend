import { Router } from "express";

abstract class BaseController {
	public abstract router: Router;
	protected abstract initializeRoutes(): void;
}

export { BaseController };
