import express, { Application } from "express";
import { ConfigType, getConfig, checkConnection, pool } from "@/configs";
import { controllers } from "@/controllers";
import { Task, CronTasks, modelsTask } from "@/cron";

class App {
	app: Application;
	config: ConfigType;

	constructor() {
		this.config = getConfig();
		this.app = express();
		this.initializeMiddlewares();
		this.initializeControllers();
		this.initializeCronTasks();
	}

	public listen() {
		const { BACKEND_PORT } = this.config;
		this.app.listen(BACKEND_PORT, () => {
			checkConnection(pool);
			console.log(`Listening on port ${BACKEND_PORT}`);
		});
	}

	private initializeControllers() {
		controllers.forEach((controller) => {
			this.app.use("/api", controller.router);
		});
	}

	private initializeCronTasks() {
		const tasks: Task[] = [modelsTask];
		const cronTasks = new CronTasks(tasks);
		cronTasks.startAllTasks();
	}

	private initializeMiddlewares() {
		this.app.use(express.json());
		this.app.use(express.urlencoded({ extended: true }));
	}
}

export default App;
